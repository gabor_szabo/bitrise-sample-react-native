import React from 'react';
import { StyleSheet, Text, View, TextInput, Button } from 'react-native';
import evaluate from './src/calculator/evaluate';

export default class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = { result: 0, text: '' };
  }

  evaluate() {
    this.setState({ result: evaluate(this.state.text) });
  }

  addText(text) {
    this.setState({ text: this.state.text + text});
  }

  clear() {
    this.setState({ result: 0, text: '' });
  }

  render() {
    return (
      <View style={styles.container}>
        <Text>Type in an expression using reverse polish notation</Text>
        <TextInput
          style={{height: 40}}
          placeholder="Reverse Polish Expression"
          onChangeText={(text) => this.setState({text})}
          value={this.state.text}
        ></TextInput>
        <Text>Result: {this.state.result}</Text>
        <View style={{flexDirection: "row", justifyContent: "space-between"}}>
          <View style={{flex: 1, margin: 2}}>
            <Button title="1" onPress={() => this.addText("1")}></Button>
          </View>
          <View style={{flex: 1, margin: 2}}>
            <Button title="2" onPress={() => this.addText("2")}></Button>
          </View>
          <View style={{flex: 1, margin: 2}}>
            <Button title="3" onPress={() => this.addText("3")}></Button>
          </View>
          <View style={{flex: 1, margin: 2}}>
            <Button title="4" onPress={() => this.addText("4")}></Button>
          </View>
        </View>
        <View style={{flexDirection: "row", justifyContent: "space-between"}}>
          <View style={{flex: 1, margin: 2}}>
            <Button title="5" onPress={() => this.addText("5")}></Button>
          </View>
          <View style={{flex: 1, margin: 2}}>
            <Button title="6" onPress={() => this.addText("6")}></Button>
          </View>
          <View style={{flex: 1, margin: 2}}>
            <Button title="7" onPress={() => this.addText("7")}></Button>
          </View>
          <View style={{flex: 1, margin: 2}}>
            <Button title="8" onPress={() => this.addText("8")}></Button>
          </View>
        </View>
        <View style={{flexDirection: "row", justifyContent: "space-between"}}>
          <View style={{flex: 1, margin: 2}}>
            <Button title="9" onPress={() => this.addText("9")}></Button>
          </View>
          <View style={{flex: 1, margin: 2}}>
            <Button title="0" onPress={() => this.addText("0")}></Button>
          </View>
          <View style={{flex: 1, margin: 2}}>
            <Button title="." onPress={() => this.addText(".")}></Button>
          </View>
          <View style={{flex: 1, margin: 2}}>
            <Button title="SPACE" onPress={() => this.addText(" ")}></Button>
          </View>
        </View>
        <View style={{flexDirection: "row", justifyContent: "space-between"}}>
          <View style={{flex: 1, margin: 2}}>
            <Button title="+" onPress={() => this.addText("+")}></Button>
          </View>
          <View style={{flex: 1, margin: 2}}>
            <Button title="-" onPress={() => this.addText("-")}></Button>
          </View>
          <View style={{flex: 1, margin: 2}}>
            <Button title="*" onPress={() => this.addText("*")}></Button>
          </View>
          <View style={{flex: 1, margin: 2}}>
            <Button title="/" onPress={() => this.addText("/")}></Button>
          </View>
        </View>
        <View style={{margin: 2}}>
          <Button title="Calculate" onPress={() => this.evaluate()}></Button>
        </View>
        <View style={{margin: 2}}>
          <Button title="Clear" onPress={() => this.clear()}></Button>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'stretch',
    justifyContent: 'center',
    paddingHorizontal: 20
  },
});
