'use strict';

import evaluate from '../src/calculator/evaluate';

describe('Reverse Polish Evaluator', () => {

  it('empty string as zero', () => {
    expect(evaluate("")).toBe(0);
  });

  it('only whitespace as zero', () => {
    expect(evaluate("  ")).toBe(0);
    expect(evaluate(" \t\n  ")).toBe(0);
  });

  it('one number as its value', () => {
    expect(evaluate("1")).toBe(1);
  });

  it('float number as its value', () => {
    expect(evaluate("1.2")).toBe(1.2);
    expect(evaluate("  1.2 ")).toBe(1.2);
  });

  it('two number addition', () => {
    expect(evaluate("1 2 +")).toBe(3);
    expect(evaluate("1\t2 +")).toBe(3);
    expect(evaluate("5    6 +")).toBe(11);
  });

  it('two number subtraction', () => {
    expect(evaluate("1 2 -")).toBe(-1);
  });

  it('two number multiplication', () => {
    expect(evaluate("1 2 *")).toBe(2);
  });

  it('two number division', () => {
    expect(evaluate("1 2 /")).toBe(0.5);
  });

  it('error for unknown operator', () => {
    expect(() => evaluate("1 2 //")).toThrowError();
  });

  it('two operators', () => {
    expect(evaluate("1 2 3 + +")).toBe(6);
    expect(evaluate("1 2 3 * -")).toBe(-5);
  });

  it('error for too many operators', () => {
    expect(() => evaluate("1 2 + +")).toThrowError();
    expect(() => evaluate("+")).toThrowError();
  });

  it('error for many numbers', () => {
    expect(() => evaluate("1 2")).toThrowError();
  });

});
